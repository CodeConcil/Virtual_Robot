﻿//========================Дмитрий Поляниченко========================
//
// Цель: определить делегаты клика, старта компиляции 
//       и закрытия  панели
//===================================================================
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delegate : MonoBehaviour 
{
	public  delegate  void  OnClick(string nameBut, bool compile, int numb);
	public static  OnClick Clickbut;


	public delegate void StartParse ();
	public static StartParse StartP;


	public delegate void Close (bool param);
	public static Close ClosePan;

}
