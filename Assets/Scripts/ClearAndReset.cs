﻿//========================Дмитрий Поляниченко========================
//
// Цель: сброс координат робота и очистка поля кода
//
//===================================================================
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClearAndReset : MonoBehaviour 
{

	public GameObject[] activeObjects = new GameObject[3]; 

	public void ClearText()
	{
		foreach (GameObject need in activeObjects)
			if (need.tag == "TextBox")
				need.GetComponent<Text>().text = "Start" + '\n';

	}


	public void ResetObj()
	{
		foreach (GameObject need in activeObjects)
			if (need.tag == "Player") 
			{
				need.transform.position = new Vector3 (0, 0.04f, 0);
				need.transform.eulerAngles = Vector3.zero;
			}
	}
}
