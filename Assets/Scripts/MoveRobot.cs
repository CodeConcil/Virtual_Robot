﻿//========================Дмитрий Поляниченко========================
//
// Цель: движение робота к цели, согласно введенным данным
//
//
// Правка: изменить реализацию поворота робота
//
//
// Текущее: баги на критических для интерполяции углах
//
//===================================================================
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveRobot : MonoBehaviour 
{

public Text textfield;
public GameObject closePan;
	   float speedmove;
	   float speedrot;
	   float path;
	   float angle;
	   Quaternion angle1;
	   bool isParse = false;

	public List<GameObject> buttons =  new List<GameObject>();

	public	List<InputField> allinputs = new List<InputField> ();
	public Transform emptydot;

	void Awake()
	{
		Delegate.Clickbut = null;
		Delegate.Clickbut += CheckButton;

		Delegate.ClosePan = null;
		Delegate.ClosePan += ControlPanel;

	}


	void Start () 
	{

		textfield.fontSize = 20;
		textfield.text = "Start" +'\n';

	}


	void InizialiseInputs()
	{
		
			foreach (InputField my in allinputs) 
			{
			
				int number;
				if (!int.TryParse (my.text, out number)) 
				{
					textfield.text = textfield.text + "Fatal Error, enter the number in field " + my.name + '\n';
					my.text = "1";
				}
		
			}

			path = int.Parse (allinputs [0].text);
			angle = int.Parse (allinputs [1].text);
			speedmove = int.Parse (allinputs [2].text);
			speedrot = int.Parse (allinputs [3].text);
	}


	void InizialiseInputs(string name, int numb)
	{


		switch (name) 
		{
			case "MoveUp": 
			{
				path = numb;
				allinputs [0].text = numb.ToString();
				break;
			}
				

			case "MoveDown": 
			{
				path = numb;
				allinputs [0].text = numb.ToString();
				break;
			}

			case "RotateLeft": 
			{
				angle = numb;
				allinputs [1].text = numb.ToString();
				break;
			}

			case "RotateRight": 
			{
				angle = numb;
				allinputs [1].text = numb.ToString();
				break;
			}
		}

		speedmove = 3;
		speedrot = 3;

		allinputs [2].text = speedmove.ToString();
		allinputs [3].text = speedrot.ToString ();

	}


	void CheckButton (string name, bool compile, int numb)
	{


		isParse = compile;
		if (!compile)
			InizialiseInputs ();
		else
			InizialiseInputs (name, numb);

		StopAllCoroutines ();

		Delegate.ClosePan (true);

		if (name == "MoveUp")
			StartCoroutine ("Move", name);
		if (name == "MoveDown")
			StartCoroutine ("Move", name);
		if (name == "RotateLeft")
			StartCoroutine ("Rotate", name);
		if (name == "RotateRight")
			StartCoroutine ("Rotate", name);


	}

	// Движение робота
	IEnumerator Move(string nameBut)
	{

		textfield.text = textfield.text + nameBut + '(' + path + ')' + '\n';

		ChangeColor (nameBut);

		if (nameBut == "MoveUp")
		{

			emptydot.eulerAngles = transform.eulerAngles;

			ChekingQuarterUp (transform, ref emptydot);


			while ((transform.position - emptydot.position).sqrMagnitude > 0.01f) 
			{

				transform.position = Vector3.Lerp(transform.position, emptydot.position, speedmove * Time.deltaTime);

			
				yield return null;
			}
			emptydot.position = transform.position;
		

		}
			

		if (nameBut == "MoveDown") 
		{
			emptydot.eulerAngles = transform.eulerAngles;

			ChekingQuarterDown (transform, ref emptydot);


			while ((transform.position - emptydot.position).sqrMagnitude > 0.01f) 
			{

				transform.position = Vector3.Lerp(transform.position, emptydot.position, speedmove * Time.deltaTime);

				yield return null;
			}
			emptydot.position = transform.position;
		}


		ReturnColor (nameBut);
		Delegate.ClosePan (false);

		if (isParse) 
		{
			Delegate.ClosePan (true);
			Delegate.StartP ();
		}

	

		yield break;
	
	}

    // Поворот робота
	IEnumerator Rotate(string nameBut)
	{
		textfield.text = textfield.text + nameBut + '('+ angle +')' + '\n';
	
		ChangeColor (nameBut);

		if (nameBut == "RotateLeft")
		{
			


			angle1 = Quaternion.Euler (0, transform.eulerAngles.y - angle, 0);


			while (Quaternion.Angle(transform.rotation, angle1)>0.01f)
			{		

				transform.rotation = Quaternion.Slerp (transform.rotation, angle1, speedrot * Time.deltaTime);

				yield return null;
			}
		}



		if (nameBut == "RotateRight") 
		{
			

			angle1 = Quaternion.Euler (0, transform.eulerAngles.y + angle, 0);


			while (Quaternion.Angle(transform.rotation, angle1)>0.001f)
			{	
				
				transform.rotation = Quaternion.Slerp (transform.rotation, angle1, speedrot * Time.deltaTime);
				yield return null;


			}


		}
			
		ReturnColor (nameBut);
		Delegate.ClosePan (false);


		if (isParse) 
		{
			Delegate.ClosePan (true);
			Delegate.StartP ();
		}


		yield break;
	}


    //Вычисление точки для интерполяции 
	void ChekingQuarterUp(Transform vectorAngle, ref Transform empty)
	{
		float zangle = 0;
		float xangle= 0;
		float zcoord = 0.0f;
		float xcoord = 0.0f;

		int normangle = Mathf.RoundToInt (vectorAngle.eulerAngles.y);

		if (normangle >= 0 && normangle <= 90) // 1-ая четверть
		{
			zangle = 90 - normangle;
			xangle = 0 - normangle;

			xangle = (xangle < 0) ? xangle * -1 : xangle;
			zangle = (zangle < 0) ? zangle * -1 : zangle;

		}

		if (normangle > 270 && normangle <= 360) // 2-ая четверть
		{
			xangle = 360 - normangle;
			zangle = 270 - normangle;


			xangle = (xangle > 0) ? xangle * -1 : xangle;
			zangle = (zangle < 0) ? zangle * -1 : zangle;


		}

		if (normangle > 180 && normangle <= 270) // 3-ая четверть
		{
			xangle = 180 - normangle;
			zangle = 270 - normangle;

			xangle = (xangle > 0) ? xangle * -1 : xangle;
			zangle = (zangle > 0) ? zangle * -1 : zangle;
		}

		if (normangle > 90 && normangle <= 180) // 4-ая четверть
		{
			zangle = 90 - normangle;
			xangle = 180 - normangle;

			zangle = (zangle > 0) ? zangle * -1 : zangle;
			xangle = (xangle < 0) ? xangle * -1 : xangle;

		}
			
		zcoord = path * Mathf.Sin (zangle *Mathf.PI/180);
		xcoord = path * Mathf.Sin (xangle * Mathf.PI/180);

		float newpathx = empty.position.x + xcoord;
		float newpathz = empty.position.z + zcoord;


		empty.position = new Vector3 (newpathx, empty.transform.position.y, newpathz);

	}

    // Вычисление точки для интерполяции при движении задом
	void ChekingQuarterDown(Transform vectorAngle, ref Transform empty)
	{
		float zangle = 0;
		float xangle= 0;
		float zcoord = 0.0f;
		float xcoord = 0.0f;

		int normangle = Mathf.RoundToInt (vectorAngle.eulerAngles.y);

		if (normangle >= 0 && normangle <= 90) // 1-ая четверть
		{
			zangle = 90 - normangle;
			xangle = 0 - normangle;

			xangle = (xangle > 0) ? xangle * -1 : xangle;
			zangle = (zangle > 0) ? zangle * -1 : zangle;

		}

		if (normangle > 270 && normangle <= 360) // 2-ая четверть
		{
			xangle = 360 - normangle;
			zangle = 270 - normangle;


			xangle = (xangle < 0) ? xangle * -1 : xangle;
			zangle = (zangle > 0) ? zangle * -1 : zangle;
		}

		if (normangle > 180 && normangle <= 270) // 3-ая четверть
		{
			xangle = 180 - normangle;
			zangle = 270 - normangle;

			xangle = (xangle < 0) ? xangle * -1 : xangle;
			zangle = (zangle < 0) ? zangle * -1 : zangle;
		}

		if (normangle > 90 && normangle <= 180) // 4-ая четверть
		{
			zangle = 90 - normangle;
			xangle = 180 - normangle;

			zangle = (zangle < 0) ? zangle * -1 : zangle;
			xangle = (xangle > 0) ? xangle * -1 : xangle;
		}

		zcoord = path * Mathf.Sin (zangle *Mathf.PI/180);
		xcoord = path * Mathf.Sin (xangle * Mathf.PI/180);

		float newpathx = empty.position.x + xcoord;
		float newpathz = empty.position.z + zcoord;


		empty.position = new Vector3 (newpathx, empty.transform.position.y, newpathz);

	}

	void ChangeColor(string nameBut)
	{

		foreach (GameObject mybut in buttons) 
		{
			if (mybut.tag == "ManagerButton" && mybut.name == nameBut) 
				mybut.GetComponent<Image> ().color = Color.red;
				
		}
	}


	void ReturnColor(string nameBut)
	{
		
		foreach (GameObject mybut in buttons) 
		{
			if (mybut.tag == "ManagerButton" && mybut.name == nameBut) 
				mybut.GetComponent<Image> ().color = Color.white;

		}
	}


	 void ControlPanel(bool isActive)
	{
		if (isActive)
			closePan.SetActive (true);
		else
			closePan.SetActive (false);
	}



}


