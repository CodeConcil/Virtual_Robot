﻿//========================Дмитрий Поляниченко========================
//
// Цель: Транслятор мнимого языка робота на рабочий аналог
//
//
// Правка: подтвердить синтаксис языка, изучить
//         API реального робота и подготовить полноценный транслятор
//
// Текущее: запись проходит без трансляции, обычная запись в файл
//
//===================================================================
using System.Collections;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using System;

public class TextfileWrite : MonoBehaviour 
{


	FileInfo sourcecode;
	StreamWriter writer;
	public Text codetext;
	List<char> code = new List<char>();


	public void Write()
	{

		try
		{
			sourcecode = new FileInfo ("SourceCode.txt");

			if (!sourcecode.Exists)
				sourcecode.Create ();
			

				writer = new StreamWriter ("SourceCode.txt");

			code.AddRange(codetext.text.ToCharArray ());

			for (int i = 0; i < code.Count; i++) 
			{
				
				if (code [i] != '\n')
					writer.Write (code [i]);

				else
					writer.WriteLine ();
			}

			writer.WriteLine("End");
			code.Clear();
			writer.Close ();
		}
		catch (Exception ex)
		{
			print (ex.Message);
		}

	}

}
