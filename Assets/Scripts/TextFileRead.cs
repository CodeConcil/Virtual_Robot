﻿//========================Дмитрий Поляниченко========================
//
// Цель: считать из файла исходный код робота и передать для
//       реализации
//
// Правка: дописать полноценный парсер, с проверкой синтаксиса
//
//
// Текущее: считываются только правильные команды
//          
//===================================================================
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

public class TextFileRead : MonoBehaviour 
{


	FileInfo sourcecode;
	StreamReader reader;
	public Text codetext;

	bool numbread = false;
	string buf = "";
	string numbbuf = "";
	string[] keys  =  {"MoveUp", "MoveDown", "RotateLeft", "RotateRight"};
	int number;
	string line;
	string outbuf  = "";


	void Awake()
	{
		Delegate.StartP = null;
		Delegate.StartP += ReadLine;
	}


	public void ReadandCompile()
	{
		sourcecode = new FileInfo ("SourceCode.txt");
		if (!sourcecode.Exists) 
		{
			codetext.text = "Error, File Not Found";
			return;
		}
		
		reader = new StreamReader ("SourceCode.txt");

		if (!reader.EndOfStream) 
		{
			line = reader.ReadLine ();
			ReadLine ();
		}


		else
			codetext.text = "Error, File empty!" +'\n' + '\n';

	}



	void ReadLine()
	{
		if (!reader.EndOfStream) {
			line = reader.ReadLine ();

			for (int i = 0; i < line.Length; i++) 
			{
				Parser (line [i]);
			}

			if (numbread) 
			{
				for (int i = 0; i < keys.Length; i++) 
				{
					if (outbuf == keys [i])
						Delegate.Clickbut (outbuf, true, int.Parse (numbbuf));

				}
				numbread = false;
			}

			if (buf == "End") 
			{
				Delegate.ClosePan (false);
				reader.Close ();
			}


			buf = "";
			numbbuf = "";
		} 

			

	}




	void Parser(char symb)
	{
		
		buf +=symb;

		for (int i=0; i<keys.Length; i++)
		{
			if (buf == keys [i]) 
			{
				numbread = true;
				outbuf = buf;
			}
		}

		if (numbread && int.TryParse (symb.ToString (), out number)) 
			numbbuf += symb;		
	}
		
}
